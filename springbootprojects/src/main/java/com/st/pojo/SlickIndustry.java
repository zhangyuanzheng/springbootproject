package com.st.pojo;

public class SlickIndustry {
    private Long industryId;

    private String industryName;

    private Long industryAppId;

    private String industryIcon;

    private Long companyId;

    private String industryIntroduce;

    public Long getIndustryId() {
        return industryId;
    }

    public void setIndustryId(Long industryId) {
        this.industryId = industryId;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName == null ? null : industryName.trim();
    }

    public Long getIndustryAppId() {
        return industryAppId;
    }

    public void setIndustryAppId(Long industryAppId) {
        this.industryAppId = industryAppId;
    }

    public String getIndustryIcon() {
        return industryIcon;
    }

    public void setIndustryIcon(String industryIcon) {
        this.industryIcon = industryIcon == null ? null : industryIcon.trim();
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getIndustryIntroduce() {
        return industryIntroduce;
    }

    public void setIndustryIntroduce(String industryIntroduce) {
        this.industryIntroduce = industryIntroduce == null ? null : industryIntroduce.trim();
    }
}