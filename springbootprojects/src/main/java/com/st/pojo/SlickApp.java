package com.st.pojo;

public class SlickApp {
    private Long appId;

    private String appName;

    private Float appPrice;

    private Long companyId;

    private String appImageAddress;

    private String appMessage;

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName == null ? null : appName.trim();
    }

    public Float getAppPrice() {
        return appPrice;
    }

    public void setAppPrice(Float appPrice) {
        this.appPrice = appPrice;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getAppImageAddress() {
        return appImageAddress;
    }

    public void setAppImageAddress(String appImageAddress) {
        this.appImageAddress = appImageAddress == null ? null : appImageAddress.trim();
    }

    public String getAppMessage() {
        return appMessage;
    }

    public void setAppMessage(String appMessage) {
        this.appMessage = appMessage == null ? null : appMessage.trim();
    }
}