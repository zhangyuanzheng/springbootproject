package com.st.filters;
import com.st.controller.JsonController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class SsoFilter implements Filter  {

    private static final Logger logger = LoggerFactory.getLogger(SsoFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
       // System.out.println("init");
        logger.info("SsoFilter init started");
    }

    @Override
    public void doFilter(ServletRequest srequest, ServletResponse sresponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) srequest;
        logger.info("this is MyFilter,url :"+request.getRequestURI());
        //不拦截swagger的请求
       // if(request.getRequestURI().contains(".html")||request.getRequestURI().contains("swagger-")||request.getRequestURI().contains("api-docs")||request.getRequestURI().contains("favicon")||request.getRequestURI().contains("map")){
            filterChain.doFilter(srequest, sresponse);
        //}else {
        //  logger.info("非法请求");
       // }

        logger.info("SsoFilter doFilter started");
    }

    @Override
    public void destroy() {
       // logger.info("destroy");
        logger.info("SsoFilter destroy started");
    }
}
