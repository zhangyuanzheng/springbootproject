package com.st.springbootprojects;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.mybatis.spring.annotation.MapperScan;

@ServletComponentScan("com.st.interceptor.MyFilter")
@ComponentScan(basePackages = {"com.st.service", "com.st.controller","com.st.config","com.st.interceptor","com.st.aop"})//组件扫描
@MapperScan("com.st.mapper")//映射扫描
@EnableCaching  //开启缓存
@SpringBootApplication//开启SpringBoot应用注解
@EnableScheduling//启用调度
@EnableAutoConfiguration//启用自动配置
@EnableTransactionManagement //可以省略 因为使用的是JDBC会默认  启用事务管理
@Configuration
public class SpringbootprojectsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootprojectsApplication.class, args);
    }

}
