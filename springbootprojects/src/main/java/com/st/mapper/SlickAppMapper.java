package com.st.mapper;

import com.st.pojo.SlickApp;

public interface SlickAppMapper {
    int deleteByPrimaryKey(Long appId);

    int insert(SlickApp record);

    int insertSelective(SlickApp record);

    SlickApp selectByPrimaryKey(Long appId);

    int updateByPrimaryKeySelective(SlickApp record);

    int updateByPrimaryKeyWithBLOBs(SlickApp record);

    int updateByPrimaryKey(SlickApp record);
}