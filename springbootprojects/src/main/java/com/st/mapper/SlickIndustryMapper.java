package com.st.mapper;

import com.st.pojo.SlickIndustry;

public interface SlickIndustryMapper {
    int deleteByPrimaryKey(Long industryId);

    int insert(SlickIndustry record);

    int insertSelective(SlickIndustry record);

    SlickIndustry selectByPrimaryKey(Long industryId);

    int updateByPrimaryKeySelective(SlickIndustry record);

    int updateByPrimaryKeyWithBLOBs(SlickIndustry record);

    int updateByPrimaryKey(SlickIndustry record);
}