package com.st.config;

import com.st.interceptor.AdminInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class LoginConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(new AdminInterceptor());
        registration.addPathPatterns("/**");                      //所有路径都被拦截
        registration.excludePathPatterns(                         //添加不拦截路径
                "json/map",            //登录
                "/*.html",
                "/js/*.js",              //js静态资源
                "/css/*.css",             //css静态资源
               "/images/*.png",
                "/images/*.jpg",
                "/fonts/*.ttf",
                "/fonts/*.woff2",
                "/fonts/*.woff"
        );
       // registry.addInterceptor(new AdminInterceptor()).addPathPatterns("/**");*/
    }

/* @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/statics/");
       // super.addResourceHandlers(registry);
    }*/
}
