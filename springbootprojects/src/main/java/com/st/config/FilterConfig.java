package com.st.config;

import com.st.filters.SsoFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.*;

@Configuration
public class FilterConfig {
//123
    @Bean
    public FilterRegistrationBean registFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new SsoFilter());
        registration.addUrlPatterns("/json");
        registration.setName("SsoFilter");
        registration.setOrder(1);
        registration.addUrlPatterns("/*");
        registration.setName("SsoFilter");
        registration.setOrder(1);
        return registration;
    }

}
