package com.st.controller;

import com.st.pojo.SlickApp;
import com.st.pojo.User;

import com.st.service.SlickAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.*;
import java.util.List;


@RestController
@RequestMapping("/json")
public class JsonController {
    private static final Logger logger = LoggerFactory.getLogger(JsonController.class);

    @Resource
    private SlickAppService slickAppService;

    @RequestMapping("/user")
    public User getUser() {
        return new User(1, "倪升武", "123456");
    }
    @RequestMapping("/list")
    public List<User> getUserList() {
        List<User> userList = new ArrayList<>();
        User user1 = new User(1, "倪升武", "123456");
        user1.getId();
        User user2 = new User(2, "达人课", "123456");
        userList.add(user1);
        userList.add(user2);
        logger.info("aaaaaaaaaaaaaaa");
        logger.debug("aass");
        return userList;
    }
    @RequestMapping("/map")
    public Map<String, Object> getMap() {
        Map<String, Object> map = new HashMap<>(3);
        User user = new User(1, "倪升武", "123456");
        map.put("作者信息", user);
        map.put("博客地址", "http://blog.itcodai.com");
        map.put("CSDN地址", null);
        map.put("粉丝数量", 4153);
       // int a=3/0;
        return map;
    }

    @RequestMapping("/insertslickApp")
    public void insert() throws SQLException {
        SlickApp slickApp=new SlickApp();
       // slickApp.setAppId(23L);
        slickApp.setAppImageAddress("123");
        slickApp.setAppName("1234");
        slickAppService.insert(slickApp);
    }
}
