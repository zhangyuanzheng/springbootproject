package com.st.service.impl;

import com.st.mapper.SlickAppMapper;
import com.st.mapper.SlickIndustryMapper;
import com.st.pojo.SlickApp;
import com.st.service.SlickAppService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.SQLException;

@Service("slickAppService")
public class SlickAppServiceImpl implements SlickAppService {

    @Resource
    private SlickAppMapper slickAppMapper;

    @Resource
    private SlickIndustryMapper slickIndustryMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insert(SlickApp record) throws SQLException {
        slickAppMapper.insertSelective(record);
       // throw new RuntimeException();
       // return 0;
      //  throw new SQLException("数据库异常");
    }
}
