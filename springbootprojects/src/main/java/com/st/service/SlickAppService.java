package com.st.service;

import com.st.pojo.SlickApp;

import java.sql.SQLException;

public interface SlickAppService {
    void insert(SlickApp record) throws SQLException;
}
