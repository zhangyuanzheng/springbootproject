package com.st.interceptor;

import com.st.filters.SsoFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdminInterceptor implements HandlerInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(AdminInterceptor.class);
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(request.getRequestURI().contains("swagger-")||request.getRequestURI().contains("api-docs")||request.getRequestURI().contains("favicon")||request.getRequestURI().contains("json")){
            logger.info("AdminInterceptor 合法请求preHandle"+request.getRequestURL());
            return true;
        }else {
            logger.info("AdminInterceptor 非法请求preHandle"+request.getRequestURL());
            return false;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        logger.info("AdminInterceptor 执行了AdminInterceptor的postHandle方法");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        logger.info("AdminInterceptor 执行了AdminInterceptor的afterCompletion方法");
    }
}
